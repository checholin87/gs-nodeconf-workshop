
const NodeCache = require('node-cache/index')
const request = require('request-promise-native')
const TWO_MINUTES = 120
const cacheInstance_ = new NodeCache({ checkperiod: TWO_MINUTES, useClones: false })

/**
async function readOrLoadData(key, uri) {
    let data = cacheInstance_.get(key)
    if (!data) {
        const promise = request.get(uri)
        data = await promise
        cacheInstance_.set(key, data)
    }
    return data

}
 */


function readOrLoadData(key, uri) {
    let data = cacheInstance_.get(key)
    if (!data ) {
        const promise = request.get(uri)
        promise
            .then(function (content) {
                cacheInstance_.set(key, JSON.stringify(JSON.parse(content)))
            })
            .catch(function (err) {
                cacheInstance_.del(key)
            });
        cacheInstance_.set(key, promise)
        data = '{}'
    } else if(data instanceof request.Request) {
        data = '{}'
    }
    return data

}

module.exports = {
    readOrLoadData:readOrLoadData
}

